Mocker
======

Installation
------------

    composer require luka8088/mocker

Usage
-----

Provides access to infinite procedurally generated database. Querying the data results
in data generation making it appear as the whole database already exists.
One of the major benefits is that the database is of infinite size and can simulate
real life huge size databases.


Example Usage:

    <?php

    require __dir__ . '/vendor/autoload.php';

    $mocker = new \luka8088\mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2');

    echo $mocker->offset('table[id].field')->word() . "\n";


Example Model:

    <?php

    require __dir__ . '/vendor/autoload.php';

    /**
     * Example mock repository.
     */
    class cityRepositoryMock {

      protected $mocker;

      function __construct ($mocker) {
        $this->mocker = $mocker;
      }

      function findOneById ($id) {
        return new cityEntityMock($this->mocker->offset('city[' . $id . ']'), $id);
      }

    }

    /**
     * Example mock entity.
     */
    class cityEntityMock {

      protected $mocker;
      protected $id;

      function __construct ($mocker, $id) {
        $this->mocker = $mocker;
        $this->id = $id;
      }

      function getId () {
        return $this->id;
      }

      function getName () {
        return $this->mocker->offset('name')->word();
      }

    }


    // Example usage:

    $cityRepository = new cityRepositoryMock(new \luka8088\mocker('wklfj298dsjk21ypqmgz398j2lkdkeo2nd2'));

    $city = $cityRepository->findOneById(1);

    echo $city->getId() . "\n";
    echo $city->getName() . "\n";
