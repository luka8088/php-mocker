<?php

namespace luka8088;

use \ErrorException as Error;

class mockerEntry {

  /** @immutable @var string */
  protected $seed = '';

  /** @immutable @var string */
  protected $offset = '';

  function __construct ($seed, $offset) {
    $this->seed = $seed;
    $this->offset = self::serializeOffset($offset);
  }

  static function serializeOffset ($offset) {
    $serializedOffset = $offset;
    if (is_array($offset)) {
      $serializedOffset = '';
      ksort($offset);
      foreach ($offset as $name => $value) {
        $serializedOffset .= $name . '=' . $value . ',';
      }
    }
    return $serializedOffset;
  }

  function offset ($offset) {
    return new mockerEntry($this->seed, $this->offset . '.' . $offset);
  }

  /**
   * Query a number among procedurally generated data.
   *
   * @param integer $variance How many variances of looked up type should there be.
   */
  function number ($variance) {
    return hexdec(substr(sha1($this->seed . $this->offset), 0, strlen(dechex($variance - 1)))) % $variance;
  }

  /**
   * Query an element from set among procedurally generated data.
   *
   * @param mixed $set Predefined set to pick from.
   */
  function element ($set) {
    return $set[$this->number(count($set))];
  }

  /**
   * Query a word among procedurally generated data.
   *
   * @param integer $minimumLetters Minimum number of letters.
   * @param integer $maximumLetters Maximum number of letters.
   */
  function word ($minimumLetters = 3, $maximumLetters = 10) {

    static $samples = [
      'beginnings' => 'a,an,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z',
      'map' => [
        'a-b', 'a-c', 'a-e', 'a-g', 'a-h', 'a-i', 'a-k', 'a-l', 'a-ll', 'a-m', 'a-n', 'a-nn',
        'a-o', 'a-q', 'a-r', 'a-rr', 'a-s', 'a-t', 'a-tt', 'a-u', 'a-v', 'a-y',
        'amp-a',
        'an-d',
        'ar-l',
        'b-a', 'b-ar', 'b-e', 'b-l', 'b-ll', 'b-o',
        'c-a', 'c-e', 'c-h', 'c-k', 'c-o', 'c-t',
        'cc-o',
        'd-a', 'd-e', 'd-h', 'd-i', 'd-o', 'd-r', 'd-u',
        'e-a', 'e-d', 'e-g', 'e-i', 'e-l', 'e-ll', 'e-m', 'e-n', 'e-o', 'e-r',
        'e-s', 'e-ss', 'e-t', 'e-v',
        'f-i', 'f-o',
        'g-a', 'g-e', 'g-h', 'g-o', 'g-u', 'g-x', 'g-z',
        'h-a', 'h-ar', 'h-e', 'h-i', 'h-n', 'h-o', 'h-u',
        'i-a', 'i-c', 'i-d', 'i-e', 'i-j', 'i-k', 'i-l', 'i-n', 'i-o', 'i-r', 'i-rn',
        'i-s', 'i-t', 'i-x', 'i-y', 'i-z',
        'j-a', 'j-e', 'j-i', 'j-o',
        'k-a', 'k-ar', 'k-e', 'k-i', 'k-o', 'k-r', 'k-s', 'k-y',
        'l-a', 'l-e', 'l-g', 'l-h', 'l-i', 'l-k', 'l-l', 'l-o', 'l-u', 'l-v', 'l-y',
        'll-a', 'll-i', 'll-y',
        'm-a', 'm-b', 'm-e', 'm-i', 'm-o', 'm-u',
        'n-a', 'n-e', 'n-g', 'n-i', 'n-j', 'n-o', 'n-t', 'n-y',
        'nn-a', 'nn-e', 'nn-i', 'nn-y',
        'o-a', 'o-b', 'o-c', 'o-cc', 'o-e', 'o-f', 'o-g', 'o-h', 'o-i', 'o-k',
        'o-l', 'o-n', 'o-nn', 'o-p', 'o-r', 'o-s', 'o-tt', 'o-u', 'o-v', 'o-w',
        'p-a', 'p-h', 'p-r',
        'pt-a', 'pt-i',
        'q-e', 'q-u',
        'r-a', 'r-c', 'r-cc', 'r-e', 'r-i', 'r-k', 'r-o', 'r-t', 'r-w', 'r-u',
        'rn-i', 'rn-a',
        'rr-o',
        's-a', 's-c', 's-e', 's-h', 's-i', 's-l', 's-m', 's-o', 's-t', 's-u',
        'ss-a',
        't-a', 't-amp', 't-e', 't-h', 't-i', 't-o', 't-r', 't-s',
        'tt-i',
        'u-a', 'u-b', 'u-c', 'u-e', 'u-k', 'u-l', 'u-m', 'u-n', 'u-pt',
        'v-a', 'v-i',
        'w-a', 'w-o',
        'x-i',
        'y-a', 'y-o', 'y-u',
        'z-h',
      ],
      'rejectedMap' => [
        'a-o-e',
        'c-k',
        'c-t-h',
        'c-t-r',
        'i-a-e',
        'k-s-l',
        'n-d',
        'o-a-o',
        'r-l',
        't-r-k-r',
        't-r-w',
      ],
    ];

    static $beginnings = [];
    static $map = [];
    static $rejectedMap = [];

    if (count($map) == 0) {

      foreach (explode(',', $samples['beginnings']) as $letter) {
        if (in_array(trim(strtolower($letter)), $beginnings))
          continue;
        $beginnings[] = trim(strtolower($letter));
      }

      foreach ($samples['map'] as $mapping) {
        list($preceding, $letter) = explode('-', $mapping);
        if (!array_key_exists(trim(strtolower($preceding)), $map)) {
          $map[trim(strtolower($preceding))] = [];
        }
        if (in_array(trim(strtolower($letter)), $map[trim(strtolower($preceding))]))
          continue;
        $map[trim(strtolower($preceding))][] = trim(strtolower($letter));
      }

      sort($beginnings);
      ksort($map);

      foreach ($map as $index => $_) {
        sort($map[$index]);
      }

      $rejectedMap = $samples['rejectedMap'];

    }

    $length = $minimumLetters + $this->number($maximumLetters - $minimumLetters);

    $letters = [];
    while (strlen(implode('', $letters)) < $length) {
      if (count($letters) == 0) {
        $letters[] = $this->offset('__letter__' . count($letters))->element($beginnings);
        continue;
      }
      if (!array_key_exists($letters[count($letters) - 1], $map)) {
        throw new Error('Mapping for *' . $letters[count($letters) - 1] . '* not found.');
        break;
      }
      $candidates = [];
      foreach ($map[$letters[count($letters) - 1]] as $candidate) {
        $acceptCandidate = true;
        foreach ($rejectedMap as $rejected) {
          if (substr('-' . implode('-', $letters) . '-' . $candidate, -strlen('-' . $rejected)) == '-' . $rejected) {
            $acceptCandidate = false;
          }
        }
        if ($acceptCandidate)
          $candidates[] = $candidate;
      }
      $letters[] = $this->offset('__letter__' . count($letters))->element($candidates);
    }

    return implode('', $letters);
  }

  /**
   * Query a text among procedurally generated data.
   *
   * @param integer $wordCount Number of words to be generated.
   */
  function text ($wordCount = 200) {
    $words = [];
    $sentenceOffset = 0;
    $wordInSentenceOffset = 0;
    for ($i = 0; $i < $wordCount; $i++) {
      if ($wordInSentenceOffset == 4 + $this->offset('__sentence__' . $sentenceOffset)->number(12)) {
        $sentenceOffset++;
        $wordInSentenceOffset = 0;
        $words[] = '.';
      }
      $word = $this->offset('__word__' . $i)->word();
      if ($wordInSentenceOffset == 0) {
        $word = ucfirst($word);
      }
      $words[] = $word;
      $wordInSentenceOffset++;
    }
    $words[] = '.';
    return str_replace(' .', '.', implode(' ', $words));
  }

}
