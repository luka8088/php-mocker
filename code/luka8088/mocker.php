<?php

namespace luka8088;

class mocker {

  /** @immutable @var string */
  protected $seed = '';

  function __construct ($seed) {
    $this->seed = $seed;
  }

  function offset ($offset) {
    return new mockerEntry($this->seed, $offset);
  }

}
